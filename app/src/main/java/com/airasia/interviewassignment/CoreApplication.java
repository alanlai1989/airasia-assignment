package com.airasia.interviewassignment;

import android.annotation.SuppressLint;
import android.app.Application;
import android.os.Looper;

import com.airasia.interviewassignment.dagger.component.ApplicationComponent;
import com.airasia.interviewassignment.dagger.component.DaggerApplicationComponent;
import com.airasia.interviewassignment.dagger.module.ApplicationModule;
import com.airasia.interviewassignment.dagger.module.NetworkModule;
import com.airasia.interviewassignment.util.Logger;

import java.util.concurrent.CountDownLatch;

import androidx.multidex.MultiDexApplication;

public class CoreApplication extends MultiDexApplication {
    private static volatile CoreApplication      singleton;
    private static          CountDownLatch       sInitLock        = new CountDownLatch(1);
    private                 CountDownLatch       mBaseContextLock = new CountDownLatch(1);
    private                 ApplicationComponent mApplicationComponent;

    public static CoreApplication getInstance() {
        if (singleton == null) {
            try {
                if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
                    @SuppressLint("PrivateApi") final Class<?> clazz = Class.forName("android.app.ActivityThread");
                    final Application application = (Application) clazz.getDeclaredMethod("currentApplication").invoke(null);
                    if (application instanceof CoreApplication) {
                        CoreApplication.initSingleton((CoreApplication) application);
                        return singleton;
                    }
                }
            } catch (Exception e) {
                Logger.logException(e);
            }

            try {
                sInitLock.await();
            } catch (InterruptedException e) {
                Logger.logException(e);
            }
        }

        return singleton;
    }

    private static void initSingleton(final CoreApplication application) {
        if (application != null) {
            singleton = application;
            sInitLock.countDown();
        }
    }

    @Override
    public void onCreate() {
        CoreApplication.initSingleton(this);
        super.onCreate();
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .networkModule(new NetworkModule())
                .build();
        mApplicationComponent.inject(this);
    }

    public ApplicationComponent getApplicationComponent() {
        if (getBaseContext() == null) {
            try {
                mBaseContextLock.await();
            } catch (InterruptedException e) {
                Logger.logException(e);
            }
        }
        return mApplicationComponent;
    }
}