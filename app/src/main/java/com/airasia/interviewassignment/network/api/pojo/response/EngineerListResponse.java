package com.airasia.interviewassignment.network.api.pojo.response;

import com.airasia.interviewassignment.vivmer.entity.Engineer;

import org.immutables.gson.Gson;
import org.immutables.value.Value;

import java.util.List;

@Gson.TypeAdapters
@Value.Immutable
public interface EngineerListResponse {
    List<Engineer> engineers();
}