package com.airasia.interviewassignment.network;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class HTTPHeaderUtils {
    private static final String DEFAULT_ACCEPTED_LANGUAGE         = "en";
    private static final String DEFAULT_ACCEPTED_LANGUAGE_COUNTRY = "en-us";

    /**
     * Obtain an accept language header for up to 5 different languages, with diminishing quality. Quality should start
     * from 1.0 and decrease by 0.1 per additional language defined. If no language is specified, this method returns
     * the en-us as default with q=1. Implemented according to: https://tools.ietf.org/html/rfc2616#page-104 Note: This
     * method perform no validation of the languages passed in
     */
    public static String getAcceptLanguageHeader() {
        // set accepted language
        final Locale defaultLocale = Locale.getDefault();
        final List<String> acceptedLanguages = new ArrayList<String>();
        if (defaultLocale != null) {
            final String languageISOCode = defaultLocale.getLanguage();
            final String languageCountry = defaultLocale.getCountry();
            if (!TextUtils.isEmpty(languageISOCode)) {
                final StringBuilder builder = new StringBuilder(languageISOCode);

                if (!TextUtils.isEmpty(languageCountry)) {
                    builder.append('-');
                    builder.append(languageCountry);
                }

                final String userAcceptedLanguage = builder.toString();
                if (HTTPHeaderUtils.DEFAULT_ACCEPTED_LANGUAGE_COUNTRY.equalsIgnoreCase(userAcceptedLanguage)
                        || HTTPHeaderUtils.DEFAULT_ACCEPTED_LANGUAGE.equalsIgnoreCase(userAcceptedLanguage)) {
                    // User choice is same as default. Just use default
                }
                else {
                    // Always add en-us and en as fallback
                    acceptedLanguages.add(userAcceptedLanguage);
                    acceptedLanguages.add(HTTPHeaderUtils.DEFAULT_ACCEPTED_LANGUAGE_COUNTRY);
                    acceptedLanguages.add(HTTPHeaderUtils.DEFAULT_ACCEPTED_LANGUAGE);
                }
            }
        }

        if (acceptedLanguages.isEmpty()) {
            acceptedLanguages.add(HTTPHeaderUtils.DEFAULT_ACCEPTED_LANGUAGE_COUNTRY);
            acceptedLanguages.add(HTTPHeaderUtils.DEFAULT_ACCEPTED_LANGUAGE);
        }

        final String QUALITY_SPECIFIER = ";q="; //$NON-NLS-1$
        final String LANGUAGES_SEPARATOR = ", "; //$NON-NLS-1$
        double quality = 1.0;

        final String decimalFormat = "%.1f"; //$NON-NLS-1$

        // Limit to max of 5 languages
        final List<String> languageSublist = acceptedLanguages.subList(0, acceptedLanguages.size() > 5 ? 5
                : acceptedLanguages.size());

        boolean firstLanguage = true;
        final StringBuilder builder = new StringBuilder();
        for (final String language : languageSublist) {
            if (!firstLanguage) {
                // Append separator first
                builder.append(LANGUAGES_SEPARATOR);
            }
            else {
                firstLanguage = false;
            }

            builder.append(language);
            builder.append(QUALITY_SPECIFIER);
            builder.append(String.format(Locale.US, decimalFormat, quality));
            quality -= 0.1;

            // Safety check. Shouldn't happen
            quality = quality < 0 ? 0 : quality;
        }

        return builder.toString();
    }
}
