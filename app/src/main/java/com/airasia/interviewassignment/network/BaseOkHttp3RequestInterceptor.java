package com.airasia.interviewassignment.network;

import java.io.IOException;

import androidx.annotation.NonNull;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class BaseOkHttp3RequestInterceptor implements Interceptor {
    @NonNull
    @Override
    public Response intercept(@NonNull final Chain chain) throws IOException {
        final Request.Builder requestBuilder = chain.request().newBuilder();
        requestBuilder.addHeader("Accept-Language", HTTPHeaderUtils.getAcceptLanguageHeader());
        return chain.proceed(requestBuilder.build());
    }
}