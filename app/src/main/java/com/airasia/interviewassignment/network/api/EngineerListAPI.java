package com.airasia.interviewassignment.network.api;

import com.airasia.interviewassignment.network.api.pojo.response.EngineerListResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface EngineerListAPI {
    @GET("path-to-get-API")
    Observable<EngineerListResponse> getEngineerList();
}