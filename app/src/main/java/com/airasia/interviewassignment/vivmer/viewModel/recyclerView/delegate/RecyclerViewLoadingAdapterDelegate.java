package com.airasia.interviewassignment.vivmer.viewModel.recyclerView.delegate;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airasia.interviewassignment.R;
import com.airasia.interviewassignment.vivmer.viewModel.recyclerView.LoadingViewModel;
import com.airasia.interviewassignment.vivmer.viewModel.recyclerView.RecyclerViewItemViewModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerViewLoadingAdapterDelegate extends ListAdapterDelegate<LoadingViewModel, RecyclerViewItemViewModel, RecyclerViewLoadingAdapterDelegate.ItemViewHolder> {
    public RecyclerViewLoadingAdapterDelegate(final LayoutInflater layoutInflater) {
        super(layoutInflater);
    }

    @Override
    protected boolean isForViewType(@NonNull final RecyclerViewItemViewModel item, final List<RecyclerViewItemViewModel> items, final int position) {
        return item instanceof LoadingViewModel;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull final ViewGroup parent) {
        final View view = mLayoutInflater.inflate(R.layout.view_loading, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull final LoadingViewModel item, @NonNull final ItemViewHolder viewHolder) {

    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {
        ItemViewHolder(final View itemView) {
            super(itemView);
        }
    }

}