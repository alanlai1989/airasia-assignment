package com.airasia.interviewassignment.vivmer.view;

import android.os.Bundle;
import android.view.MenuItem;

import com.airasia.interviewassignment.CoreApplication;
import com.airasia.interviewassignment.util.Logger;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import io.reactivex.plugins.RxJavaPlugins;

public abstract class BaseActivity extends AppCompatActivity {
    protected abstract int getLayoutResource();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CoreApplication.getInstance()
                .getApplicationComponent()
                .inject(this);
        onSetContentView();
        setRxJavaErrorHandler();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void onSetContentView() {
        setContentView(getLayoutResource());
    }

    private void setRxJavaErrorHandler() {
        if (RxJavaPlugins.getErrorHandler() != null && !RxJavaPlugins.isLockdown()) {
            RxJavaPlugins.setErrorHandler(Logger::logException);
        }
    }
}