package com.airasia.interviewassignment.vivmer.viewModel;

import com.airasia.interviewassignment.vivmer.entity.Engineer;

class EngineerItemViewModelImpl implements com.airasia.interviewassignment.vivmer.viewModel.EngineerItemViewModel {
    private final Engineer mEngineer;

    EngineerItemViewModelImpl(final Engineer engineer) {
        mEngineer = engineer;
    }

    @Override
    public String getName() {
        return mEngineer.name();
    }
}