package com.airasia.interviewassignment.vivmer.viewModel;

import android.content.Context;

import com.airasia.interviewassignment.vivmer.entity.Engineer;
import com.airasia.interviewassignment.vivmer.viewModel.recyclerView.RecyclerViewItemViewModel;

import org.apache.commons.collections4.ListUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import io.reactivex.Observable;

public class EngineerScheduleViewModelImpl implements EngineerScheduleViewModel {
    private static final int TOTAL_DAYS = 14;

    private final Context        mContext;
    private final List<Engineer> mEngineerList;

    private final ObservableBoolean mRefreshing  = new ObservableBoolean();
    private final ObservableBoolean mShowRefresh = new ObservableBoolean();

    private ObservableArrayList<RecyclerViewItemViewModel> mViewModels;

    private List<List<Engineer>> mEngineerScheduleList = new ArrayList<>();

    public EngineerScheduleViewModelImpl(final Context context, final List<Engineer> engineerList) {
        mContext = context;
        mEngineerList = engineerList;
        mViewModels = new ObservableArrayList<>();
        generateScheduleAndPopulate();
    }

    @Override
    public ObservableArrayList<RecyclerViewItemViewModel> getViewModels() {
        return mViewModels;
    }

    @Override
    public Observable<List<? extends RecyclerViewItemViewModel>> loadPage(final int page) {
        return Observable.empty();
    }

    @Override
    public SwipeRefreshLayout.OnRefreshListener getOnRefreshListener() {
        return null;
    }

    @Override
    public ObservableBoolean getRefreshing() {
        return mRefreshing;
    }

    @Override
    public ObservableBoolean getShouldShowRefresh() {
        return mShowRefresh;
    }

    private void generateScheduleAndPopulate() {
        if (mEngineerScheduleList.size() >= TOTAL_DAYS) {
            for (int i = 0; i < TOTAL_DAYS; i++) {
                EngineerScheduleItemViewModel viewModel = new EngineerScheduleItemViewModelImpl(mContext, i + 1, mEngineerScheduleList.get(i));
                getViewModels().add(viewModel);
            }
            return;
        }
        Collections.shuffle(mEngineerList);
        mEngineerScheduleList.addAll(ListUtils.partition(new ArrayList<>(mEngineerList), 2));
        generateScheduleAndPopulate();
    }
}