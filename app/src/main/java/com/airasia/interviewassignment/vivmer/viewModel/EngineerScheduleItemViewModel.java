package com.airasia.interviewassignment.vivmer.viewModel;

import com.airasia.interviewassignment.vivmer.viewModel.recyclerView.RecyclerViewItemViewModel;

public interface EngineerScheduleItemViewModel extends RecyclerViewItemViewModel {
    String getNumberOfDay();

    String getMorningEngineer();

    String getNightEngineer();
}