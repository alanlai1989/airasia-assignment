package com.airasia.interviewassignment.vivmer.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.airasia.interviewassignment.util.GsonUtil;
import com.google.gson.JsonSyntaxException;

import org.immutables.gson.Gson;
import org.immutables.value.Value;

@Gson.TypeAdapters
@Value.Immutable
public abstract class Engineer implements Parcelable {
    public abstract long id();

    public abstract String name();

    // ----- Boilerplate for Parcelable ----- //
    public static final Parcelable.Creator<Engineer> CREATOR = new Parcelable.Creator<Engineer>() {
        @Override
        public Engineer createFromParcel(final Parcel parcel) {
            return fromJsonString(parcel.readString());
        }

        @Override
        public Engineer[] newArray(final int i) {
            return new Engineer[0];
        }
    };

    @Override
    public void writeToParcel(final Parcel parcel, final int flag) {
        parcel.writeString(toJsonString());
    }

    public String toJsonString() {
        return GsonUtil.getGson().toJson(this, Engineer.class);
    }

    public static Engineer fromJsonString(final String json) {
        try {
            return GsonUtil.getGson().fromJson(json, Engineer.class);
        } catch (final JsonSyntaxException e) {
            return null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }
}