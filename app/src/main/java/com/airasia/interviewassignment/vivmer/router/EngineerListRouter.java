package com.airasia.interviewassignment.vivmer.router;

import android.content.Intent;

import com.airasia.interviewassignment.vivmer.entity.Engineer;
import com.airasia.interviewassignment.vivmer.view.BaseActivity;
import com.airasia.interviewassignment.vivmer.view.EngineerScheduleActivity;

import java.util.ArrayList;

public class EngineerListRouter {
    public static final String EXTRA_ENGINEER_LIST = "EXTRA_ENGINEER_LIST";

    private final BaseActivity mActivity;

    public EngineerListRouter(BaseActivity activity) {
        mActivity = activity;
    }

    public void navigateToEngineerSchedule(ArrayList<Engineer> engineerList) {
        Intent intent = new Intent(mActivity, EngineerScheduleActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(EXTRA_ENGINEER_LIST, engineerList);
        mActivity.startActivity(intent);
    }
}