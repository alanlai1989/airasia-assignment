package com.airasia.interviewassignment.vivmer.viewModel;

import com.airasia.interviewassignment.network.api.pojo.response.EngineerListResponse;
import com.airasia.interviewassignment.util.Logger;
import com.airasia.interviewassignment.vivmer.entity.Engineer;
import com.airasia.interviewassignment.vivmer.interactor.EngineerInteractor;
import com.airasia.interviewassignment.vivmer.viewModel.recyclerView.LoadingViewModel;
import com.airasia.interviewassignment.vivmer.viewModel.recyclerView.RecyclerViewItemViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class EngineerListViewModelImpl implements com.airasia.interviewassignment.vivmer.viewModel.EngineerListViewModel {
    private final EngineerInteractor mInteractor;

    private final LoadingViewModel  mLoadingViewModel = new LoadingViewModel();
    private final ObservableBoolean mRefreshing       = new ObservableBoolean(true);
    private final ObservableBoolean mShowRefresh      = new ObservableBoolean(true);

    private ObservableArrayList<RecyclerViewItemViewModel> mViewModels;

    private List<Engineer> mEngineerList;

    public EngineerListViewModelImpl(final EngineerInteractor interactor) {
        mInteractor = interactor;
        mViewModels = new ObservableArrayList<>();
        loadFirstPage();
    }

    @Override
    public ObservableArrayList<RecyclerViewItemViewModel> getViewModels() {
        return mViewModels;
    }

    @Override
    public Observable<List<? extends RecyclerViewItemViewModel>> loadPage(final int page) {
        mRefreshing.set(true);
        mShowRefresh.set(page == 1);
        if (page == 1) {
            mViewModels.clear();
        }
        else {
            for (RecyclerViewItemViewModel viewModel : mViewModels) {
                if (viewModel instanceof LoadingViewModel) {
                    return retrieveData(page);
                }
            }
            mViewModels.add(mLoadingViewModel);
        }
        return retrieveData(page);
    }

    @Override
    public SwipeRefreshLayout.OnRefreshListener getOnRefreshListener() {
        return () -> loadFirstPage();
    }

    @Override
    public ObservableBoolean getRefreshing() {
        return mRefreshing;
    }

    @Override
    public ObservableBoolean getShouldShowRefresh() {
        return mShowRefresh;
    }

    @Override
    public List<Engineer> getEngineerList() {
        return mEngineerList;
    }

    private Observable<List<? extends RecyclerViewItemViewModel>> retrieveData(final int page) {
        return mInteractor.getEngineerList()
                .flatMap((Function<EngineerListResponse, ObservableSource<List<? extends RecyclerViewItemViewModel>>>) engineerListResponse -> {
                    List<EngineerItemViewModel> viewModelList = new ArrayList<>();
                    if (engineerListResponse.engineers() != null && !Objects.requireNonNull(engineerListResponse.engineers()).isEmpty()) {
                        mEngineerList = engineerListResponse.engineers();
                        for (Engineer engineer : engineerListResponse.engineers()) {
                            EngineerItemViewModel viewModel = new EngineerItemViewModelImpl(engineer);
                            viewModelList.add(viewModel);
                        }
                    }
                    return Observable.just(viewModelList);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(recyclerViewItemViewModels -> {
                    mRefreshing.set(false);
                    mShowRefresh.set(false);
                    mViewModels.remove(mLoadingViewModel);
                })
                .doOnError(throwable -> {
                    Logger.logException(throwable);
                    mViewModels.remove(mLoadingViewModel);
                    mShowRefresh.set(false);
                });
    }

    private void loadFirstPage() {
        loadPage(1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(recyclerViewItemViewModels -> {
                    getViewModels().addAll(recyclerViewItemViewModels);
                }, Logger::logException);
    }
}