package com.airasia.interviewassignment.vivmer.viewModel;

import com.airasia.interviewassignment.vivmer.viewModel.recyclerView.RecyclerViewItemViewModel;

public interface EngineerItemViewModel extends RecyclerViewItemViewModel {
    String getName();
}