package com.airasia.interviewassignment.vivmer.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;

import com.airasia.interviewassignment.CoreApplication;
import com.airasia.interviewassignment.R;
import com.airasia.interviewassignment.dagger.module.EngineerListModule;
import com.airasia.interviewassignment.databinding.ActivityEngineerListBinding;
import com.airasia.interviewassignment.vivmer.interactor.EngineerInteractor;
import com.airasia.interviewassignment.vivmer.router.EngineerListRouter;
import com.airasia.interviewassignment.vivmer.viewModel.EngineerListViewModel;
import com.airasia.interviewassignment.vivmer.viewModel.EngineerListViewModelImpl;
import com.airasia.interviewassignment.vivmer.viewModel.recyclerView.RecyclerViewAdapter;
import com.airasia.interviewassignment.vivmer.viewModel.recyclerView.RecyclerViewDividerDecoration;
import com.airasia.interviewassignment.vivmer.viewModel.recyclerView.RecyclerViewItemViewModel;
import com.airasia.interviewassignment.vivmer.viewModel.recyclerView.delegate.EngineerListAdapterDelegate;
import com.airasia.interviewassignment.vivmer.viewModel.recyclerView.delegate.RecyclerViewLoadingAdapterDelegate;
import com.hannesdorfmann.adapterdelegates2.AdapterDelegatesManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

public class EngineerListActivity extends BaseActivity {
    @Inject
    EngineerInteractor mInteractor;

    @Inject
    EngineerListRouter mRouter;

    private ActivityEngineerListBinding mBinding;
    private EngineerListViewModel       mViewModel;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_engineer_list;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CoreApplication.getInstance()
                .getApplicationComponent()
                .plus(new EngineerListModule(this))
                .inject(this);
        bindData();
        setup();
        setupRecyclerView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.engineer_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_view_schedule) {
            mRouter.navigateToEngineerSchedule(new ArrayList<>(mViewModel.getEngineerList()));
        }
        return super.onOptionsItemSelected(item);
    }

    private void bindData() {
        mViewModel = new EngineerListViewModelImpl(mInteractor);
        mBinding = DataBindingUtil.setContentView(this, getLayoutResource());
        mBinding.setViewModel(mViewModel);
    }

    private void setup() {
        setSupportActionBar(mBinding.actionBar.toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle(R.string.engineer_list_title);
    }

    private void setupRecyclerView() {
        LayoutInflater inflater = LayoutInflater.from(this);
        AdapterDelegatesManager<List<RecyclerViewItemViewModel>> manager = new AdapterDelegatesManager<>();
        manager.addDelegate(new RecyclerViewLoadingAdapterDelegate(inflater));
        manager.addDelegate(new EngineerListAdapterDelegate(inflater));
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(manager, mViewModel.getViewModels());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mBinding.recyclerView.setLayoutManager(layoutManager);
        mBinding.recyclerView.setAdapter(adapter);
        mBinding.recyclerView.addItemDecoration(new RecyclerViewDividerDecoration(this, R.drawable.divider_transparent_small));
    }
}
