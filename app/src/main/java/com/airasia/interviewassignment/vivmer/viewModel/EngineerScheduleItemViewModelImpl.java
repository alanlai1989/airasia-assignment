package com.airasia.interviewassignment.vivmer.viewModel;

import android.content.Context;

import com.airasia.interviewassignment.R;
import com.airasia.interviewassignment.vivmer.entity.Engineer;

import java.util.List;

public class EngineerScheduleItemViewModelImpl implements EngineerScheduleItemViewModel {
    private final Context        mContext;
    private final int            mNumberOfDay;
    private final List<Engineer> mEngineers;

    public EngineerScheduleItemViewModelImpl(final Context context,
                                             final int numberOfDay,
                                             final List<Engineer> engineers) {
        mContext = context;
        mNumberOfDay = numberOfDay;
        mEngineers = engineers;
    }

    @Override
    public String getNumberOfDay() {
        return mContext.getString(R.string.day, mNumberOfDay);
    }

    @Override
    public String getMorningEngineer() {
        return mContext.getString(R.string.morning, mEngineers.get(0).name());
    }

    @Override
    public String getNightEngineer() {
        return mContext.getString(R.string.night, mEngineers.get(1).name());
    }
}