package com.airasia.interviewassignment.vivmer.viewModel.recyclerView.delegate;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airasia.interviewassignment.R;
import com.airasia.interviewassignment.databinding.ViewEngineerScheduleItemBinding;
import com.airasia.interviewassignment.vivmer.viewModel.EngineerScheduleItemViewModel;
import com.airasia.interviewassignment.vivmer.viewModel.recyclerView.RecyclerViewItemViewModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class EngineerScheduleAdapterDelegate extends ListAdapterDelegate<EngineerScheduleItemViewModel, RecyclerViewItemViewModel, EngineerScheduleAdapterDelegate.ItemViewHolder> {
    public EngineerScheduleAdapterDelegate(final LayoutInflater layoutInflater) {
        super(layoutInflater);
    }

    @Override
    protected boolean isForViewType(@NonNull final RecyclerViewItemViewModel item, final List<RecyclerViewItemViewModel> items, final int position) {
        return item instanceof EngineerScheduleItemViewModel;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull final ViewGroup parent) {
        return new ItemViewHolder(mLayoutInflater.inflate(R.layout.view_engineer_schedule_item, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull final EngineerScheduleItemViewModel item, @NonNull final ItemViewHolder viewHolder) {
        final ViewEngineerScheduleItemBinding binding = viewHolder.mBinding;
        binding.setViewModel(item);
        binding.executePendingBindings();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        ViewEngineerScheduleItemBinding mBinding;

        ItemViewHolder(final View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}