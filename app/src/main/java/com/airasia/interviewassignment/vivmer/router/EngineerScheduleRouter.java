package com.airasia.interviewassignment.vivmer.router;

import android.content.Intent;

import com.airasia.interviewassignment.vivmer.view.BaseActivity;
import com.airasia.interviewassignment.vivmer.view.EngineerListActivity;

public class EngineerScheduleRouter {
    private final BaseActivity mActivity;

    public EngineerScheduleRouter(BaseActivity activity) {
        mActivity = activity;
    }

    public void navigateToEngineerListing() {
        Intent intent = new Intent(mActivity, EngineerListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mActivity.startActivity(intent);
    }
}