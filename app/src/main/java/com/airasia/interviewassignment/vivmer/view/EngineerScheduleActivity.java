package com.airasia.interviewassignment.vivmer.view;

import android.os.Bundle;
import android.view.LayoutInflater;

import com.airasia.interviewassignment.CoreApplication;
import com.airasia.interviewassignment.R;
import com.airasia.interviewassignment.dagger.module.EngineerScheduleModule;
import com.airasia.interviewassignment.databinding.ActivityEngineerScheduleBinding;
import com.airasia.interviewassignment.vivmer.entity.Engineer;
import com.airasia.interviewassignment.vivmer.router.EngineerListRouter;
import com.airasia.interviewassignment.vivmer.viewModel.EngineerScheduleViewModel;
import com.airasia.interviewassignment.vivmer.viewModel.EngineerScheduleViewModelImpl;
import com.airasia.interviewassignment.vivmer.viewModel.recyclerView.RecyclerViewAdapter;
import com.airasia.interviewassignment.vivmer.viewModel.recyclerView.RecyclerViewDividerDecoration;
import com.airasia.interviewassignment.vivmer.viewModel.recyclerView.RecyclerViewItemViewModel;
import com.airasia.interviewassignment.vivmer.viewModel.recyclerView.delegate.EngineerScheduleAdapterDelegate;
import com.airasia.interviewassignment.vivmer.viewModel.recyclerView.delegate.RecyclerViewLoadingAdapterDelegate;
import com.hannesdorfmann.adapterdelegates2.AdapterDelegatesManager;

import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

public class EngineerScheduleActivity extends BaseActivity {
    private ActivityEngineerScheduleBinding mBinding;
    private EngineerScheduleViewModel       mViewModel;

    private List<Engineer> mEngineerList;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_engineer_schedule;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CoreApplication.getInstance()
                .getApplicationComponent()
                .plus(new EngineerScheduleModule(this))
                .inject(this);
        getInputData();
        bindData();
        setup();
        setupRecyclerView();
    }

    private void getInputData() {
        mEngineerList = getIntent().getParcelableArrayListExtra(EngineerListRouter.EXTRA_ENGINEER_LIST);
    }

    private void bindData() {
        mViewModel = new EngineerScheduleViewModelImpl(this, mEngineerList);
        mBinding = DataBindingUtil.setContentView(this, getLayoutResource());
        mBinding.setViewModel(mViewModel);
    }

    private void setup() {
        setSupportActionBar(mBinding.actionBar.toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.engineer_schedule_title);
    }

    private void setupRecyclerView() {
        LayoutInflater inflater = LayoutInflater.from(this);
        AdapterDelegatesManager<List<RecyclerViewItemViewModel>> manager = new AdapterDelegatesManager<>();
        manager.addDelegate(new RecyclerViewLoadingAdapterDelegate(inflater));
        manager.addDelegate(new EngineerScheduleAdapterDelegate(inflater));
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(manager, mViewModel.getViewModels());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mBinding.recyclerView.setLayoutManager(layoutManager);
        mBinding.recyclerView.setAdapter(adapter);
        mBinding.recyclerView.addItemDecoration(new RecyclerViewDividerDecoration(this, R.drawable.divider_transparent_small));
    }
}
