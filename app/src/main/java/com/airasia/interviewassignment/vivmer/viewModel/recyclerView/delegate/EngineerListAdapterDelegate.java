package com.airasia.interviewassignment.vivmer.viewModel.recyclerView.delegate;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airasia.interviewassignment.R;
import com.airasia.interviewassignment.databinding.ViewEngineerItemBinding;
import com.airasia.interviewassignment.vivmer.viewModel.EngineerItemViewModel;
import com.airasia.interviewassignment.vivmer.viewModel.recyclerView.RecyclerViewItemViewModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class EngineerListAdapterDelegate extends ListAdapterDelegate<EngineerItemViewModel, RecyclerViewItemViewModel, EngineerListAdapterDelegate.ItemViewHolder> {
    public EngineerListAdapterDelegate(final LayoutInflater layoutInflater) {
        super(layoutInflater);
    }

    @Override
    protected boolean isForViewType(@NonNull final RecyclerViewItemViewModel item, final List<RecyclerViewItemViewModel> items, final int position) {
        return item instanceof EngineerItemViewModel;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull final ViewGroup parent) {
        return new ItemViewHolder(mLayoutInflater.inflate(R.layout.view_engineer_item, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull final EngineerItemViewModel item, @NonNull final ItemViewHolder viewHolder) {
        final ViewEngineerItemBinding binding = viewHolder.mBinding;
        binding.setViewModel(item);
        binding.executePendingBindings();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        ViewEngineerItemBinding mBinding;

        ItemViewHolder(final View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}