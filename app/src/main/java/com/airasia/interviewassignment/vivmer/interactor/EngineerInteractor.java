package com.airasia.interviewassignment.vivmer.interactor;

import com.airasia.interviewassignment.network.api.EngineerListAPI;
import com.airasia.interviewassignment.network.api.pojo.response.EngineerListResponse;

import io.reactivex.Observable;

public class EngineerInteractor {
    private final EngineerListAPI mApi;

    public EngineerInteractor(final EngineerListAPI api) {
        mApi = api;
    }

    public Observable<EngineerListResponse> getEngineerList() {
        return mApi.getEngineerList();
    }
}