package com.airasia.interviewassignment.vivmer.viewModel;

import com.airasia.interviewassignment.vivmer.entity.Engineer;
import com.airasia.interviewassignment.vivmer.viewModel.recyclerView.RecyclerViewViewModel;

import java.util.List;

public interface EngineerListViewModel extends RecyclerViewViewModel {
    public List<Engineer> getEngineerList();
}