package com.airasia.interviewassignment.util;

import com.airasia.interviewassignment.CoreApplication;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class FixtureLoaderUtil {
    private FixtureLoaderUtil() {
        // Ensure Singleton
    }

    public static <T> T fixtureFromName(final String fixtureName, final Class<T> fixtureClass) {
        String json;
        try {
            InputStream is = CoreApplication.getInstance().getAssets().open("json/" + fixtureName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            Logger.logException(ex);
            return null;
        }
        final Gson gson = GsonUtil.getGson();
        return gson.fromJson(json, fixtureClass);
    }
}