package com.airasia.interviewassignment.util;

import com.airasia.interviewassignment.network.api.pojo.response.GsonAdaptersEngineerListResponse;
import com.airasia.interviewassignment.vivmer.entity.GsonAdaptersEngineer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonUtil {
    private static Gson GSON;

    private GsonUtil() {
        // Ensure Singleton
    }

    public static synchronized Gson getGson() {
        if (GSON == null) {
            final GsonBuilder gsonBuilder = new GsonBuilder();

            //POJO
            gsonBuilder.registerTypeAdapterFactory(new GsonAdaptersEngineerListResponse());

            //MODEL
            gsonBuilder.registerTypeAdapterFactory(new GsonAdaptersEngineer());

            GSON = gsonBuilder.create();
        }
        return GsonUtil.GSON;
    }
}
