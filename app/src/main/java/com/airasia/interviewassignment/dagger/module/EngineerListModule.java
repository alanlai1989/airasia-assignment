package com.airasia.interviewassignment.dagger.module;

import com.airasia.interviewassignment.network.api.EngineerListAPI;
import com.airasia.interviewassignment.vivmer.interactor.EngineerInteractor;
import com.airasia.interviewassignment.vivmer.router.EngineerListRouter;
import com.airasia.interviewassignment.vivmer.view.BaseActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class EngineerListModule {
    private final BaseActivity mActivity;

    public EngineerListModule(BaseActivity activity) {
        mActivity = activity;
    }

    @Provides
    EngineerInteractor provideEngineerInteractor(final EngineerListAPI api) {
        return new EngineerInteractor(api);
    }

    @Provides
    EngineerListRouter provideEngineerListRouter() {
        return new EngineerListRouter(mActivity);
    }
}