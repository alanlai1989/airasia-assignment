package com.airasia.interviewassignment.dagger.module;

import android.content.Context;

import com.airasia.interviewassignment.CoreApplication;
import com.airasia.interviewassignment.util.GsonUtil;
import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {
    private final CoreApplication mApplication;

    public ApplicationModule(final CoreApplication application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    CoreApplication provideApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    Context provideApplicationContext(CoreApplication application) {
        return application;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return GsonUtil.getGson();
    }
}