package com.airasia.interviewassignment.dagger.component;

import android.content.Context;

import com.airasia.interviewassignment.CoreApplication;
import com.airasia.interviewassignment.dagger.module.APIModule;
import com.airasia.interviewassignment.dagger.module.ApplicationModule;
import com.airasia.interviewassignment.dagger.module.EngineerListModule;
import com.airasia.interviewassignment.dagger.module.EngineerScheduleModule;
import com.airasia.interviewassignment.dagger.module.NetworkModule;
import com.airasia.interviewassignment.vivmer.view.BaseActivity;
import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class, APIModule.class})
public interface ApplicationComponent {
    CoreApplication getApplication();

    Retrofit getRetrofit();

    Context getApplicationContext();

    Gson getGson();

    void inject(CoreApplication application);

    void inject(BaseActivity baseActivity);

    EngineerListComponent plus(EngineerListModule engineerListModule);

    EngineerScheduleComponent plus(EngineerScheduleModule engineerScheduleModule);
}
