package com.airasia.interviewassignment.dagger.component;

import com.airasia.interviewassignment.dagger.module.EngineerListModule;
import com.airasia.interviewassignment.vivmer.view.EngineerListActivity;

import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = {EngineerListModule.class})
public interface EngineerListComponent {
    void inject(EngineerListActivity activity);
}