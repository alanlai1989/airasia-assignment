package com.airasia.interviewassignment.dagger.module;


import com.airasia.interviewassignment.network.api.EngineerListAPI;
import com.airasia.interviewassignment.network.api.pojo.response.EngineerListResponse;
import com.airasia.interviewassignment.util.FixtureLoaderUtil;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Observable;
import retrofit2.Retrofit;

@Module
public class APIModule {
    @Provides
    @Singleton
    EngineerListAPI provideEngineerListAPI(Retrofit retrofit) {
        return () -> Observable.just(FixtureLoaderUtil.fixtureFromName("engineering_list.json", EngineerListResponse.class));
    }
}