package com.airasia.interviewassignment.dagger.module;

import com.airasia.interviewassignment.vivmer.router.EngineerScheduleRouter;
import com.airasia.interviewassignment.vivmer.view.BaseActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class EngineerScheduleModule {
    private final BaseActivity mActivity;

    public EngineerScheduleModule(BaseActivity activity) {
        mActivity = activity;
    }

    @Provides
    EngineerScheduleRouter provideEngineerScheduleRouter() {
        return new EngineerScheduleRouter(mActivity);
    }
}