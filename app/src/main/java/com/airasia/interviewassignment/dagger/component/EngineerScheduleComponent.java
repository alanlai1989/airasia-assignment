package com.airasia.interviewassignment.dagger.component;

import com.airasia.interviewassignment.dagger.module.EngineerScheduleModule;
import com.airasia.interviewassignment.vivmer.view.EngineerScheduleActivity;

import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = {EngineerScheduleModule.class})
public interface EngineerScheduleComponent {
    void inject(EngineerScheduleActivity activity);
}